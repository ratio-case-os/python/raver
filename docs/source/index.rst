.. include:: ../../README.rst

.. toctree::
   :caption: Appendix
   :maxdepth: 1

   Changelog <./changelog>
   Reference <./autoapi/index>
   Index <genindex>
