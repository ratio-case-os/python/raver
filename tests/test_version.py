from pathlib import Path

import semver
from raver.cli import (
    check_changelog,
    check_ref_increments,
    get_current_ref,
    get_module_version,
    get_ref_text,
    resolve_git_path,
)


def test_resolve_git_path(here, cwd):
    with cwd(here):
        assert resolve_git_path(here) == Path("./tests")


def test_current_ref():
    assert get_current_ref()


def test_get_current_version(here):
    txt = (here / "raw/v.py").read_text()
    assert get_module_version(txt) == semver.VersionInfo(1, 2, 3, "pre", "build")


def test_get_reference_version(here, cwd):
    with cwd(here):
        get_module_version(
            get_ref_text(here.parent / "raver/__init__.py", "origin/master")
        )


def test_compare_versions():
    v1 = semver.VersionInfo(1, 2, 3)
    v2 = semver.VersionInfo(1, 3, 0)
    v3 = semver.VersionInfo(2, 3, 4)

    check_ref_increments(v2, v1, True)
    check_ref_increments(v3, v2, True)
    check_ref_increments(v1, v2, True)


def test_check_changelog(here):
    check_changelog(here / "raw/log.rst", semver.VersionInfo(1, 2, 3))
